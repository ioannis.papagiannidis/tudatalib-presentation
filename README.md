# TUdatalib presentation slides

Live version of current slides [here](https://ioannis.papagiannidis.pages.rwth-aachen.de/tudatalib-presentation/slides.html#/)

To compile locally:
1. Clone repo
```
git clone https://git.rwth-aachen.de/ioannis.papagiannidis/tudatalib-presentation.git 
```
2. Compile markdown file to HTML
```
pandoc -t revealjs -s -o slides.html slides.md
    -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/
    -V theme=moon
    -V --slide-level=2
```
HTML file should have been produced in the folder now.

You can change the theme option by picking a [new one](https://revealjs.com/themes/).

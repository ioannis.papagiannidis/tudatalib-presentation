---
author: Ioannis Papagiannidis, Tomislav Maric, Z-INF (CRC1194)
title: TUdatalib
date: June 10, 2020
---

# Introduction

## What is it?
- It's a repository for all research data generated or worked with at TU Darmstadt
- Storage of data and metadata
- Long-term archiving (10 years+)
- Publication of metadata and files incl. assignment of DOIs
- Granular rights management based on the membership in the research groups

## Community
- Refers to the Faculty.
- Example: Mathematik, Physik, Chemie, Biologie
- Sub-communities refer for example to different research groups
- Example: Geometrie und Approximation, Logik, Mathematical Modelling and Analysis

## Collection
- A set of data items with a common title.
- A collection is not published, nor is it necessarily publicly accessible, only data items are published.
- Example 1: a scientific article may have a collection with the title of the article, and different data sets from the results section.
- Example 2: a collection can be a scientific study, that contains different data sets.

## Data Item
- Data items are basically files/archives of files that contain data.
- Can also be definition files or even complete Docker/Singularity images.

# Archiving data

## 1. Login in using TU ID

## 2. Find your community (Fachbereich)
![](images/community-list.png){height=540px}

## 3. Create a Collection
![](images/create-collection.png)

## 4. Upload data items into your collection
![](images/data-item-upload.png)

## 5. Fill out the information
- about your data items
- i.e. title, description, authors, type of content, upload date, etc.

# Publishing data

## 1. Select the data item that you want to publish
![](images/select-data-item.png)

## 2. Click on "Edit Item"
![](images/edit-data-item.png)

## 3. Make the data and metadata public
- By removing access restrictions
- And registering a DOI to use in your article
![](images/remove-restrictions-reserve-DOI.png)

